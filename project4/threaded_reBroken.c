#include <pcap/pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#define max 5000
#include <errno.h>
#include <string.h>
#include <inttypes.h>
#include "spooky.h"

struct PacketHolder {
	char bIsValid;		// 0 if no, 1 if yes
//	char byData[2000];	// The actual packet data
	uint32_t nHash;		// Hash of the packet contents
};

struct arrayContents {
	size_t length;		// payload length
	char data[2000];	// the data
};

void put(struct arrayContents);	// put, get, producer, consumer functions
struct arrayContents get();
void* DumpAllPacketLengths();
void* computeHash();

int isInArray(uint32_t, struct PacketHolder*, int);
void calculateRedundancy(float);

struct arrayContents buffer[max];		// producer/consumer buffer
int count = 0, fill = 0, use = 0, args = 0;	// variables to keep track of conditions
float countRedundancy = 0, countTotal = 0;

pthread_mutex_t m;				//mutex and cond vars
pthread_cond_t condC, condP;

FILE * fp;					// so can read files from producer function (Dump)

int main(int argc, char *argv[]) {
    int i, nThreads = 2;		// need to write to take nThreads from args
    char * pcap_files[10];
    int nConThreads = nThreads / 2;
    int nProThreads = nThreads - nConThreads;

    for (i = 1; i < argc; i++) {
        pcap_files[i - 1] = argv[i];
    }
    for (i = 1; i < argc; i++) {
        fp = fopen(pcap_files[i - 1], "r");
        if (fp == NULL) {
            printf("Error opening file: %s\n", strerror(errno));
        }
        else {
	    fseek(fp, 24, SEEK_CUR);

	    pthread_t con[nConThreads];
	    pthread_t pro[nProThreads];

	    pthread_mutex_init(&m, NULL);				// initialize threads
	    pthread_cond_init(&condC, NULL);
	    pthread_cond_init(&condP, NULL);

	    for (i = 0; i < nProThreads; i++) {
		pthread_create(&pro[i], NULL, DumpAllPacketLengths, NULL);// Run producer w nProThreads
	    }

	    for (i = 0; i < nConThreads; i++) { 
	    	pthread_create(&con[i], NULL, computeHash, NULL);// run consumer w nConThreads
	    }

	    for (i = 0; i < nProThreads; i++) {
	    	pthread_join(pro[i], NULL);
	    }
	    

	    for (i = 0; i < nConThreads; i++) {
		pthread_join(con[i], NULL);
	    }
        }

	pthread_cond_destroy(&condC);
	pthread_cond_destroy(&condP);
	pthread_mutex_destroy(&m);
    }

    /* Temporary using 1 mutex, 2 cond vars */

    calculateRedundancy(countRedundancy);	// count and print redundancy

    return 0;
}

void* DumpAllPacketLengths () {
    struct arrayContents PCdata;	// data to be used in prducer consumer array
    uint32_t    nPacketLength;
    char        theData[2000];

    while(!feof(fp)) {
        // Skip the ts_sec field
        fseek(fp, 4, SEEK_CUR);

        // Skip the ts_usec field
        fseek(fp, 4, SEEK_CUR);

        // Read in the incl_len field
        fread(&nPacketLength, 4, 1, fp);

        // Skip the orig_len field
        fseek(fp, 4, SEEK_CUR);

        // Check if package is too large or too small (<128 bytes)
        if(nPacketLength < 2000 && nPacketLength >= 128) {
            //printf("Packet length was %d\n", nPacketLength);

			// ignore bytes 0-51 of package
	    fseek(fp, 51, SEEK_CUR);
	    size_t payloadLength = nPacketLength - 51;

            int r = fread(theData, 1, payloadLength, fp);

	    PCdata.length = payloadLength;	// Put data in struct
	    strcpy(PCdata.data, theData);

	    pthread_mutex_lock(&m);		// lock before accessing buffer
	    
	    while (count == max) {		// wait while full
		printf("waiting");
	    	pthread_cond_wait(&condP, &m);
	    }

	    put(PCdata);			// put the data and signal
	    pthread_cond_signal(&condC);
	    pthread_mutex_unlock(&m);		// unlock buffer

            if (r != payloadLength) {
                printf("Error in reading current package \n");
	    }
	 } 
	 else {
    	    	if (nPacketLength < 128) {
        		printf("Skipping %d bytes ahead - packet is too small\n", nPacketLength);
     			fseek(fp, nPacketLength, SEEK_CUR);
        	} 
		else {
	         	printf("Skipping %d bytes ahead - packet is too big\n", nPacketLength);
    	        	fseek(fp, nPacketLength, SEEK_CUR);
		}
	     }

    	}
    pthread_exit(0);
}

void* computeHash() {
	int i;
	struct PacketHolder g_MyBigTable[30000];
	
	for (i = 0; i <= countTotal; i++) {
		pthread_mutex_lock(&m);				// lock buffer before getting data
	
		while (count == 0) {				// wait while empty
			pthread_cond_wait(&condC, &m);
		}
	
		struct arrayContents PCdata;			// get data and signal
		PCdata = get();

		pthread_cond_signal(&condP);	
		pthread_mutex_unlock(&m);			// unlock mutex
	
		//uint32_t hashy = spooky_hash32((const void *)theData, buffer[i].length, 0);
		uint32_t hashy = spooky_hash32(PCdata.data, PCdata.length, 0); 		

	        if (isInArray(hashy, g_MyBigTable, 30000) != 0) {
					// compare .byData with theData and if the same, do nothing
			countRedundancy++;	// if in table, count as redundant
	        } else {
	          		//g_MyBigTable[i].byData = theData;
	       		g_MyBigTable[i].nHash 		= hashy;
	        	g_MyBigTable[i].bIsValid	= 1;
	       	}
		
		countTotal++;	// count total hashes placed

	}

	pthread_exit(0);
}

int isInArray(uint32_t hashVal, struct PacketHolder *myArr, int size) {
	int i = 0;
	while (myArr[i].bIsValid == 1) {
		if (myArr[i].nHash == hashVal) { return i; }
		i++;
	}
	return 0;
}

void put(struct arrayContents PCdata) {
	buffer[fill] = PCdata;	// fill buffer and update fill index
	fill = (fill+1) % max;
	count++;		// increase count in buffer
}

struct arrayContents get() {
	struct arrayContents PCdata;
	PCdata = buffer[use];	// get struct from buffer and update use index
	use = (use+1) % max;
	count--;		// decrease count in buffer
	return PCdata;
}	

void calculateRedundancy(float redundant) {
	printf("Redundancy: %f%", redundant/countTotal);
}
