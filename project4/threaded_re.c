#include <pcap/pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>
#include "spooky.h"
#include <pthread.h>

/* Struct Definition */
struct PacketHolder {
	char bIsValid;		// 0 if no, 1 if yes
	char byData[2000];	// The actual packet data
	uint32_t nHash;		// Hash of the packet contents
};

/* Global Variables */
struct PacketHolder g_MyBigTable[100000];
int i = 0;
int numHashes = 0;
int numPackets = 0;
int bytesHit = 0;
int bytesProcessed = 0;
pthread_cond_t prod, cons;
pthread_mutex_t mp, mc;
int buffer = 0;

/* Function Declarations */
void DumpAllPacketLengths(FILE * fp);
int isInArray(uint32_t haash, struct PacketHolder *arrray, int sz);
void computeHashes();

/* Main Function */
int main(int argc, char *argv[]) {
    int i = 1;
    FILE * fp;
    char * pcap_files[10];
    int level = 1;
    int nThreads;
    int numFiles = 1;

	/* Commenting out threading */
    //int num = 0;

	/* initialize mutex and condition varables */
  /*  pthread_mutex_init(&mp, NULL);
    pthread_mutex_init(&mc, NULL);
    pthread_cond_init(&cons, NULL);
    pthread_cond_init(&prod, NULL); */

	/* Parse Command Line Args */
	while (i < argc) {
		if (strcmp(argv[i], "-level") == 1) { i++; level = argv[i]; }
		else if (strcmp(argv[i], "-thread") == 1) { i++; nThreads = (int)argv[i]; }
		else { pcap_files[i-1] = argv[i]; numFiles++; }
		i++;
	}

    for (i = 1; i < numFiles; i++) {
        fp = fopen(pcap_files[i - 1], "r");
        if (fp == NULL) {
            printf("Error opening file: %s\n", strerror(errno));
        }
        else {
            fseek(fp, 24, SEEK_CUR);
            /* Commenting out Threading */
         /*   pthread_t pro[argc-1], con[argc-1];
            for (num = 0; num < argc-1; num++) {
            	printf("Calling pro thread %d\n", num);
	            pthread_create(&pro[num], NULL, (void *)DumpAllPacketLengths, fp);
			}
			for (num = 0; num < argc-1; num++) {
				printf("Calling con thread %d\n", num);
				pthread_create(&con[num], NULL, (void *)computeHashes, NULL);
			}
			for (num = 0; num < argc-1; num++) {
				printf("Waiting on pro %d\n", num);
				pthread_join(pro[num], NULL);
			}
			for (num = 0; num < argc-1; num++) {
				printf("Waiting on con %d\n", num);
				buffer = 1;
				pthread_join(con[num], NULL);
			}
		*/
            DumpAllPacketLengths(fp);
			computeHashes();
        }
    }

	/* Commenting out Threading */
	/* Cleanup */
	/*pthread_mutex_destroy(&mp);
	pthread_mutex_destroy(&mc);
	pthread_cond_destroy(&cons);
	pthread_cond_destroy(&prod); */
	double redundancy = (double)bytesHit/(double)bytesProcessed;
	printf("Results:\nBytes Processed: %d\nHits: %d\nRedundancy: %f\n", bytesProcessed, numHashes, redundancy);
    return 0;
}

void DumpAllPacketLengths (FILE * fp) {
    uint32_t    	nPacketLength;
    char        	theData[2000];
    int i = 0;

    while(!feof(fp) && i < 100000) {
    	/* Commenting out Threading
    	printf("Locking mutex in producer %d with buffer %d\n", i, buffer);
		pthread_mutex_lock(&mp);
		while (buffer == 0) { pthread_cond_wait(&prod, &mp); } // wait if there is something in the buffer
		*/
		//buffer = i;

        // Skip the ts_sec field
        fseek(fp, 4, SEEK_CUR);

        // Skip the ts_usec field
        fseek(fp, 4, SEEK_CUR);

        // Read in the incl_len field
        fread(&nPacketLength, 4, 1, fp);

        // Skip the orig_len field
        fseek(fp, 4, SEEK_CUR);

        // Check if package is too large or too small (<128 bytes)
        if(nPacketLength < 2000 && nPacketLength >= 128) {
           // printf("Packet length was %d\n", nPacketLength);
			// ignore bytes 0-51 of package
			fseek(fp, 51, SEEK_CUR);
			size_t payloadLength = nPacketLength - 51;
			bytesProcessed = bytesProcessed + (int)payloadLength;

			// read data, hash it, and store it in array
            int r = fread(theData, 1, payloadLength, fp);
			strncpy(g_MyBigTable[i].byData, theData, sizeof(theData));
			g_MyBigTable[i].bIsValid = 1;

            if (r != payloadLength && r != 0) {
                printf("Error in reading current package \n");
            }
            numPackets++;
            i++;
        }
        else {
        	if (nPacketLength < 128) {
        	//	printf("Skipping %d bytes ahead - packet is too small\n", nPacketLength);
        		fseek(fp, nPacketLength, SEEK_CUR);
        	} else {
	        //    printf("Skipping %d bytes ahead - packet is too big\n", nPacketLength);
    	        fseek(fp, nPacketLength, SEEK_CUR);
			}
        }
      /*  pthread_cond_signal(&cons);
        pthread_mutex_unlock(&mp);
        printf("Unlocked mutex in producer %d with buffer %d\n", i, buffer); */

    }
	//pthread_exit(0);

}

void computeHashes(){
	int index = 0;
	while (g_MyBigTable[index].bIsValid == 1) {
	/*	printf("Locking mutex consumer %d\n", index);
		pthread_mutex_lock(&mc);
		buffer = 0;
		while (buffer != 0) { pthread_cond_wait(&cons, &mc); }
	*/	uint32_t hashy = spooky_hash32((const void *)g_MyBigTable[index].byData, (size_t)strlen(g_MyBigTable[index].byData), 0);
		if (isInArray(hashy, g_MyBigTable, 30000) != 0) { }
		else {
			g_MyBigTable[index].nHash = hashy;
			numHashes++;
			bytesHit = bytesHit + strlen(g_MyBigTable[index].byData);
		}
		index++;
		//buffer = 0;
	/*	pthread_cond_signal(&cons);
		pthread_mutex_unlock(&mc); */
	}
	//pthread_exit(0);
//	pthread_cond_signal(&prod);
//	pthread_mutex_unlock(&m);
}


//int isInArray(uint32_t hashVal, char *myData, struct PacketHolder *myArr, int size) {
int isInArray(uint32_t hashVal, struct PacketHolder *myArr, int size) {
	int i = 0;
	while (myArr[i].bIsValid == 1) {
		if (myArr[i].nHash == hashVal) //{ return i; }
		{
		//	if (memcmp(myArr[i].byData, myData, sizeof(myData))) {	// check if actual data is a match
				return i;
		//	}
		}
		i++;
	}
	return 0;
}


