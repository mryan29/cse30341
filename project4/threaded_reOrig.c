#include <pcap/pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>
#include "spooky.h"

void DumpAllPacketLengths (FILE * fp);
//int isInArray(uint32_t, char *, struct PacketHolder *, int);
struct PacketHolder {
	char bIsValid;		// 0 if no, 1 if yes
	char byData[2000];	// The actual packet data
	uint32_t nHash;		// Hash of the packet contents
};

struct PacketHolder g_MyBigTable[30000];
int i = 0;
int numHashes = 0;

int isInArray(uint32_t haash, struct PacketHolder *arrray, int sz);
void computeHash(char *myData, size_t ploadLength, int index);
int main(int argc, char *argv[]) {
    int i;
    FILE * fp;
    char * pcap_files[10];

    for (i = 1; i < argc; i++) {
        pcap_files[i - 1] = argv[i];
    }
    for (i = 1; i < argc; i++) {
        fp = fopen(pcap_files[i - 1], "r");
        if (fp == NULL) {
            printf("Error opening file: %s\n", strerror(errno));
        }
        else {
            fseek(fp, 24, SEEK_CUR);
            DumpAllPacketLengths(fp);
        }
    }

    return 0;
}

void DumpAllPacketLengths (FILE * fp) {
//	struct PacketHolder g_MyBigTable[30000];
  	//struct PacketHolder *g_MyBigTable = malloc(30000 * sizeof(struct PacketHolder));
    uint32_t    	nPacketLength;
    char        	theData[2000];
    int i = 0;

    while(!feof(fp)) {
        // Skip the ts_sec field
        fseek(fp, 4, SEEK_CUR);

        // Skip the ts_usec field
        fseek(fp, 4, SEEK_CUR);

        // Read in the incl_len field
        fread(&nPacketLength, 4, 1, fp);

        // Skip the orig_len field
        fseek(fp, 4, SEEK_CUR);

        // Check if package is too large or too small (<128 bytes)
        if(nPacketLength < 2000 && nPacketLength >= 128) {
            //printf("Packet length was %d\n", nPacketLength);
			//i++;
			// ignore bytes 0-51 of package
			fseek(fp, 51, SEEK_CUR);
			size_t payloadLength = nPacketLength - 51;

			// read data, hash it, and store it in array
            int r = fread(theData, 1, payloadLength, fp);

			computeHash(theData, payloadLength, i);
/*            uint32_t hashy = spooky_hash32((const void *)theData, payloadLength, 0);

          	//if (isInArray(hashy, theData, g_MyBigTable, 30000) != 0) {
			if (isInArray(hashy, g_MyBigTable, 30000) != 0) {
				// compare .byData with theData and if the same, do nothing

          	} else {
          		//g_MyBigTable[i].byData 		= theData;
          	//	strncpy(g_MyBigTable[i].byData, theData, sizeof(theData));
          		g_MyBigTable[i].nHash 		= hashy;
          		g_MyBigTable[i].bIsValid	= 1;
          	}
*/
            if (r != payloadLength && r != 0) {
                printf("Error in reading current package \n");
            }
            i++;
        }
        else {
        	if (nPacketLength < 128) {
        	//	printf("Skipping %d bytes ahead - packet is too small\n", nPacketLength);
        		fseek(fp, nPacketLength, SEEK_CUR);
        	} else {
	         //   printf("Skipping %d bytes ahead - packet is too big\n", nPacketLength);
    	        fseek(fp, nPacketLength, SEEK_CUR);
			}
        }

    }
//for testing purposes:
    i = 0;
    while (g_MyBigTable[i].bIsValid == 1) {
    	printf("%" PRIu32 "\n", g_MyBigTable[i].nHash);
    	i++;
    }
//    printf("Num Total Packets: %d\n", i);
  //  printf("Num Hashes: %d\n", numHashes);

}

void computeHash(char *myData, size_t ploadLength, int index) {
	uint32_t hashy = spooky_hash32((const void *)myData, ploadLength, 0);

	if (isInArray(hashy, g_MyBigTable, 30000) != 0) {

	} else {
		g_MyBigTable[index].nHash		= hashy;
		g_MyBigTable[index].bIsValid	= 1;
		numHashes++;
	}
}

//int isInArray(uint32_t hashVal, char *myData, struct PacketHolder *myArr, int size) {
int isInArray(uint32_t hashVal, struct PacketHolder *myArr, int size) {
	int i = 0;
	while (myArr[i].bIsValid == 1) {
		if (myArr[i].nHash == hashVal) //{ return i; }
		{
		//	if (memcmp(myArr[i].byData, myData, sizeof(myData))) {	// check if actual data is a match
				return i;
		//	}
		}
		i++;
	}
	return 0;
}


