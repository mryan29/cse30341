We first read a given file's packets' contents using fread and storing it into the .byData attributes of a corresponding struct inside our big array.
To compute the hashes, we use SpookyHash (taken from online as approved by Prof. Striegal) and check that the hash and corresponding data does not already 
exist inside our big array.
Finally, we compute redundancy by dividing the bytes of hits by the total bytes processed.

We were not able to properly implement threading unfortunately so in an effort to have at least something working, we've commented those bits out.


