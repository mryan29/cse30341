/*
Main program for the virtual memory project.
Make all of your modifications to this file.
You may add or rearrange any code or data as you need.
The header files page_table.h and disk.h explain
how to use the page table and disk interfaces.
*/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include <map>

#include "page_table.h"
#include "disk.h"
#include "program.h"

using namespace std;

vector<int> entries (150, 0);    // keeps track of free frames
int counter = 0;    			// counter for fifo
map<int, int> frame_map;        // keeps track to which page each frame is mapped to
								// index = frame num, entry = page num
struct disk *disk;				// virtual disk object
int pgFaultCount = 0, readCount = 0, writeCount = 0;

int check_free_frames(int nframes) {
    for (int i = 0; i < nframes; i++) {
        if (entries[i] == 0) {
            return(i);
        }
    }
    return(-1);   
}

void init_map (map<int, int> frame_map, int nframes) {
    for (int i = 0; i < nframes; i++) {
        frame_map[i] = -1;
    }
}

void page_fault_handler(struct page_table *pt, int page)
{
    cout << "asdas" << endl;
    printf("page fault on page #%d\n", page);
    page_table_set_entry(pt,page,page,PROT_READ|PROT_WRITE);
}

void fifo_handler(struct page_table *pt, int page) {
    printf("page fault on page #%d\n", page);
    pgFaultCount += 1;
    int frame;
    int bits;
    int nframes = page_table_get_nframes(pt);	// nframes = total num frames in vm
    char *physmem = page_table_get_physmem(pt);	// physmem = ptr to start of pt in pm

    /*for (int i = 0; i < nframes; i++) {
        cout << "frame:" << i << "page:" << frame_map[i] << endl;
    }*/
    page_table_get_entry(pt, page, &frame, &bits); // get frame num and access bits assoc w page
    
    int new_frame = counter % nframes;
    cout << "new frame" << new_frame << endl;

    if ((bits & PROT_READ) == 1) {      // Add write permission
        page_table_set_entry(pt, page, frame, PROT_READ|PROT_WRITE);
    }
    else {      // Add read permission
        if (frame_map[new_frame] == 0) {      // if chosen frame is empty
            page_table_set_entry(pt, page, new_frame, PROT_READ);
            disk_read(disk, page, &physmem[new_frame*PAGE_SIZE]);
	    readCount += 1;
            frame_map[new_frame] = page + 1;
        }
        else {      // Frame is not empty
            int p = frame_map[new_frame] - 1;
            page_table_get_entry(pt, p, &frame, &bits);
            if ((bits & PROT_WRITE) == 1) {     // Check dirty bit
                disk_write(disk, p, &physmem[new_frame*PAGE_SIZE]);
		writeCount += 1;
                disk_read(disk, page, &physmem[new_frame*PAGE_SIZE]);
		readCount += 1;
                page_table_set_entry(pt, page, new_frame, PROT_READ);
                page_table_set_entry(pt, p, 0, 0);
                frame_map[new_frame] = page + 1;
            }
            else {
                disk_read(disk, page, &physmem[new_frame*PAGE_SIZE]);
		readCount += 1;
                page_table_set_entry(pt, page, new_frame, PROT_READ);
                page_table_set_entry(pt, p, 0, 0);
                frame_map[new_frame] = page + 1;
            }
        }
        counter++;
    }
}

void rand_handler(struct page_table *pt, int page) {
    printf("page fault on page #%d\n", page);
    cout << "rand" << endl;
    pgFaultCount += 1;
    int frame;
    int bits;
    int nframes = page_table_get_nframes(pt);
    int npages = page_table_get_npages(pt);
    int free_frame = check_free_frames(nframes);
    char *physmem = page_table_get_physmem(pt);

    page_table_get_entry(pt, page, &frame, &bits);
    if ((bits & PROT_READ) == 1) {      // Add write permission
        page_table_set_entry(pt, page, frame, PROT_READ|PROT_WRITE);
        disk_write(disk, page, &physmem[frame*PAGE_SIZE]);
        writeCount += 1;
    }
    else {      // Add read permission
        if (free_frame > -1) {      // if there are still free frames
            page_table_set_entry(pt, page, free_frame, PROT_READ);
            entries[free_frame] = 1;
            disk_read(disk, page, &physmem[free_frame*PAGE_SIZE]);
	    readCount += 1;
        }
        else {      // No more free frames; Randomly choose frame to remove
            int p = rand() % npages;
            page_table_get_entry(pt, p, &frame, &bits);
            if ((bits & PROT_WRITE) == 1) {     // Check dirty bit
                //disk_write(disk, p, &physmem[frame*PAGE_SIZE]);
		//writeCount += 1;
                disk_read(disk, page, &physmem[frame*PAGE_SIZE]);
		readCount += 1;
                page_table_set_entry(pt, page, frame, PROT_READ);
                page_table_set_entry(pt, p, 0, 0);
            }
            else {
                disk_read(disk, page, &physmem[frame*PAGE_SIZE]);
		readCount += 1;
                page_table_set_entry(pt, page, frame, PROT_READ);
                page_table_set_entry(pt, p, 0, 0);
            }
        }
    }
}

int main(int argc, char *argv[])
{

	/* Parse Command Line Args */
    if (argc != 5) {
    	printf("use: virtmem <npages> <nframes> <rand|fifo|custom> <sort|scan|focus>\n");
    	return 1;
    }
 
    int npages = atoi(argv[1]);
    int nframes = atoi(argv[2]);
    const char *algorithm = argv[3];
    const char *program = argv[4];
    srand(time(NULL));
    //init_map(frame_map, nframes);

    if ( (npages < 3) || (nframes < 3)) {
        cout << "The minimum number of pages and frames is 3." << endl;;
        exit(1);
    }

	/* Create Virtual Disk */ 
	// npages Blocks
    disk = disk_open("myvirtualdisk", npages);	// returns ptr to new disk object
    if (!disk) {	// if fails to create new virtual disk
    	fprintf(stderr, "couldn't create virtual disk: %s\n", strerror(errno));
   	 	return 1;
    }
 
    char *virtmem; 
    char *physmem;
    struct page_table *pt;

	/* Create Page Table */ 
	// VM npages Big and PM nframes Big That Calls _handler on Page Faults
    if (strcmp(algorithm, "fifo") == 0) {
        pt = page_table_create(npages, nframes, fifo_handler);
    }
    
    if (strcmp(algorithm, "rand") == 0) {
        pt = page_table_create(npages, nframes, rand_handler);
    }

   /* else {
        pt = page_table_create(npages, nframes, page_fault_handler);   
    }*/
    if (!pt) {
    	fprintf(stderr, "couldn't create page table: %s\n", strerror(errno));
    	return 1;
    }

    virtmem = page_table_get_virtmem(pt);
    physmem = page_table_get_physmem(pt);

    if (!strcmp(program, "sort")) {
    sort_program(virtmem, npages * PAGE_SIZE);

    } else if (!strcmp(program, "scan")) {
    scan_program(virtmem, npages * PAGE_SIZE);

    } else if (!strcmp(program, "focus")) {
    focus_program(virtmem, npages * PAGE_SIZE);

    } else {
    fprintf(stderr, "unknown program: %s\n", argv[3]);
    return 1;
    }

    printf("Page faults: %d\n", pgFaultCount);
    printf("Disk reads: %d\n", readCount);
    printf("Disk writes: %d\n", writeCount);

    page_table_delete(pt);
    disk_close(disk);

    return 0;
}

