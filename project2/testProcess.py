#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3.6
# -*- coding: UTF-8 -*-

# Reference: https://stackoverflow.com/questions/3393612/run-certain-code-every-n-seconds

import sys  # for parsing cmd line args
import time

def loopPrint():
	while True:
		print("I am the process print every 5 seconds!\n")
		time.sleep(5)

loopPrint()
