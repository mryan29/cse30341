// NETID(s): mzanardo, mryan29

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <string>
#include <cstring>
#include <iostream>
#include <vector>		// to hold child pids
#include <algorithm>	// to check if pid is in pid vector
#include <zmq.hpp>
using namespace std;

// Current issues: Feb13 9:00pm
	// 'fg': is it ok that it just returns the PID, and not any signals or anything like that? is this the return code?
	// for "Handles stopping a stopped process" do we need to output a specific message
	// for "Handles continuing a running process" do we need to output a specific message
	// need to check permissions "Handles a command where incorrect permissions are present"

// Global Variables
vector<int> pids;
int counter = 0;
int child_pid;
string port = "62600";
string url = "tcp://*:62600";
const char * level;
zmq::context_t context(1);
zmq::socket_t socket(context, ZMQ_REP);

// Function Prototypes
void bg(char * arguments);				// bg command
void stop(int pid);						// stop command
void fg(int pid);						// fg command
void cont(int pid);						// continue command
void killChildren();					// terminate child processes
void help();							// help command
bool checkPID(int pid);					// check pid is one of ours
void signal_func (char * sig, int pid);	// signal command
void list_func (vector<int> pids);		// list command
void list_func_v2(vector<int> pids);

int main(int argc, char *argv[]) {

	cout << "Welcome to irish.\n";
	level = argv[2];


	// attempted level 2 functionality - not really working anymore, unsure why
	if (strcmp(level,"2") == 0) {
		cout << "Level 2 version.\n";
		zmq::context_t context(1);
		zmq::socket_t socket(context, ZMQ_REP);
		socket.bind(url);
		while (true) {
			zmq::message_t request;
			socket.recv(&request);
			char *rq = static_cast<char*>(request.data());
			char *rqst;
			rqst = strtok(rq, "\n");
			rqst = strtok(rq, " ");

			if (strcmp(rqst, "bg") == 0) {
				bg(rqst);
				continue;
			}
			if (strcmp(rqst, "list") == 0) {
				list_func_v2(pids);
				continue;
			}
			if (strcmp(rqst, "fg") == 0) {
				int pid = atoi(strtok(NULL, " "));
				if (checkPID(pid)) {
					fg(pid);
				} else {
					zmq::message_t reply(62);
					memcpy(reply.data(), "Given PID is not a child process of irish. Please try again.\n", 62);
					socket.send(reply);
				}
				continue;
			}
			if (strcmp(rqst, "signal") == 0) {
				int pid = atoi(strtok(NULL, " "));
				char * sig = strtok(NULL, " ");
				 if (checkPID(pid)) {
				 	signal_func(sig, pid);
				 } else {
				 	zmq::message_t reply(62);
				 	memcpy(reply.data(), "Given PID is not a child process of irish. Please try again.\n", 62);
				 	socket.send(reply);
				 }
				continue;
			}
			if (strcmp(rqst, "stop") == 0) {
				int pid = atoi(strtok(NULL, " "));
				if (checkPID(pid)) {
					stop(pid);
				} else {
					zmq::message_t reply(62);
					memcpy(reply.data(), "Given PID is not a child process of irish. Please try again.\n", 62);
					socket.send(reply);
				}
				continue;
			}

			sleep(1);
		}
	}

	// level 1 functionality - working
	else { cout << "Level 1 version.\n"; }

	while(1) {

        cout << ">  ";	// simulate terminal prompt
        // initialize input array and read in from stdin
        char input[100];
        fgets(input, 50, stdin);

        // initialize args and use spaces/newline as delimiters
        char * args;

        args = strtok(input, "\n");
        args = strtok(input, " ");

        // depending on user input, execute corresponding function
        if (strcmp(args , "bg") == 0) {
        	bg(args);
            continue;
        }

        if (strcmp(args , "list") == 0) {
            list_func(pids);
			continue;
        }

        if (strcmp(args , "fg") == 0) {
        	int pid = atoi(strtok(NULL, " "));
        	if (checkPID(pid)) {
	        	fg(pid);
	        } else {
	        	cout << "Given PID is not a child process of irish. Please try again.\n";
	        }
            continue;
        }

        if (strcmp(args , "signal") == 0) {
            int pid = atoi(strtok(NULL, " "));
            char * sig = strtok(NULL, " ");
            if (checkPID(pid)) {
				signal_func(sig, pid);
			} else {
				cout << "Given PID is not a child process of irish. Please try again.\n";
			}
            continue;
        }

        if (strcmp(args , "stop") == 0) {
        	int pid = atoi(strtok(NULL, " "));
        	if (checkPID(pid)) {
	        	stop(pid);
			} else {
				cout << "Given PID is not a child process of irish. Please try again.\n";
			}
        	continue;
        }

        if (strcmp(args , "continue") == 0) {
        	int pid = atoi(strtok(NULL, " "));
        	if (checkPID(pid)) {
				cont(pid);
    		} else {
    			cout << "Given PID is not a child process of irish. Please try again.\n";
    		}
            continue;
        }

        if (strcmp(args , "quit") == 0) {
			killChildren();
            exit(1);
        }

		if (strcmp(args , "help") == 0) {
			help();
		}

        else {
            cout << "Invalid command." << endl;
            continue;
        }


    }

    return 0;
}

void bg(char * arguments) {
	char *myargs[3];
	arguments = strtok(NULL, " ");
	myargs[0] = arguments;			// myargs[0]: python call
	arguments = strtok(NULL, " ");
	myargs[1] = arguments;			// myargs[1]: name of python program to run
	myargs[2] = NULL;				// marks end of command
	int rc = fork();

	if (rc < 0) {       // Fork failed
		cerr << "Fork failed" << endl;
		exit(EXIT_FAILURE);
	}
	else if (rc != 0) {      // parent process
		child_pid = rc;
		pids.push_back(child_pid);		// attempting to add pids to pids vector
	}
	else if (rc == 0) {		// Child process
        child_pid = getpid();
		pids.push_back(child_pid);

      //  if (strcmp(level, "2") != 0) {	// level 1 functionality
		if (strcmp(level, "1") == 0) {
            cout << "Process " << child_pid << " started in the background.\n";
        }

		// attempting to add level 2 functionality here
        else {
            zmq::message_t reply(42);
            string str = "Process "+to_string(child_pid)+" started in the background.\n";
            const void *a = str.c_str();
            memcpy(reply.data(), a, 42);
            socket.send(reply);
        }
		counter++;
		execvp(myargs[0], myargs);
	}
}

void stop(int pid) {
	int s = kill(pid, 19);
	if (s == 0) {
		cout << "Process " << pid << " paused." << endl;
	}
	else if (errno != 0) { // check if command fails
		cout << strerror(errno) << endl;
	}
}

void fg(int pid) {
	pid = (pid_t)pid;
	pids.push_back(pid);
	int status;

	// check process is running before waiting on process
	pid_t w = waitpid((pid_t)pid, &status, WNOHANG | WUNTRACED | WCONTINUED);
	if (w != 0) {
		cout << "Process must be running. Please try again.\n";
	} else {
		pid_t wc = waitpid(pid, NULL, 0);	// wait on the process to finish
		(void)wc;							// to eliminate the "unused variable" warning when compiling for wc
		if (WIFEXITED(status)) { cout << "Process " << pid << " exited with status " << WEXITSTATUS(status) << endl; }
		else if (WIFSIGNALED(status)) { cout << "Process " << pid << " killed by signal " << WTERMSIG(status) <<  endl; }
		else if (WIFSTOPPED(status)) { cout << "Process " << pid << " stopped by signal " << WSTOPSIG(status) <<  endl; }
		else if (WIFCONTINUED(status)) { cout << "Process " << pid << " contined" << endl; }
	}

	if (errno != 0) { // check if command fails
		cout << strerror(errno) << endl;
	}
}

void cont(int pid) {
	int s = kill(pid, 18);

	 if (s == 0) {
	 	cout << "Process " << pid << " continued." << endl;
	 }
	 else if (errno != 0) { // check if command fails
	 	cout << strerror(errno) << endl;
	 }

}

void killChildren() {
	pid_t w, pid;
	int status;

	cout << "Terminating running child processes...\n";

	// iterate through pids vector, terminating any running processes
	for (auto it = pids.begin(); it != pids.end(); it++){
		pid = *it;
		w = waitpid((pid_t)pid, &status, WNOHANG | WUNTRACED | WCONTINUED);
		if (w == 0) {
			status = kill(pid, 9);
		}
	}

}

void help() {
	cout << "Please enter one of the commands below.\n";
	cout << "\tlist\t\t\tprint list of active child processes spawned by shell\n";
	cout << "\tbg COMMAND\t\texecute external command asynchronously in the background\n";
	cout << "\tfg PID\t\t\twait for previously running command in the background to terminate, display return code\n";
	cout << "\tsignal PID SIGNAL\tsignals process PID an arbitrary signal SIGNAL\n";
	cout << "\tstop PID\t\t\tsignals process PID to stop\n";
	cout << "\tcontinue PID\t\tsignals process PID to continue\n";
	cout << "\tquit\t\t\tmanually exit, forcibly terminate any remaining child processes\n";
	cout << "\thelp\t\t\tprints help message about all of the commands\n\n";
}

bool checkPID(int pid) {
	// if given pid is in pids vector, return true. else, return false
	if (find(pids.begin(), pids.end(), pid) != pids.end()) {
		return true;
	} else {
		return false;
	}

}

void signal_func (char * sig, int pid) {

    int s;
    if (strcmp(sig , "SIGHUP") == 0) {
        s = kill(pid, 1);
    }

    if (strcmp(sig , "SIGINT") == 0) {
        s = kill(pid, 2);
    }

    if (strcmp(sig , "SIGQUIT") == 0) {
        s = kill(pid, 3);
    }

    if (strcmp(sig , "SIGSTOP") == 0) {
  		s = kill(pid, 19);
    }

    if (strcmp(sig , "SIGKILL") == 0) {
        s = kill(pid, 9);
    }

    if (strcmp(sig , "SIGALARM") == 0) {
        s = kill(pid, 14);
    }

    if (strcmp(sig , "SIGCONT") == 0) {
        s = kill(pid, 18);
    }

    if (s == 0) {
        cout << "Success." << endl;
    }
    else if (errno != 0 ) {
        cout << strerror(errno) << endl;
    }

}

void list_func (vector<int> pids) {
    pid_t w, pid;
    int status;
	if (pids.empty()) { cout << "No child processes.\n"; }
	// iterate through pids vector, printing table with pid and state
	else {
 	   cout << "   pid:" << "     state: " << endl;

 	   for (auto it = pids.begin(); it != pids.end(); it++){
 	       pid = *it;
    	    w = waitpid((pid_t)pid, &status, WNOHANG | WUNTRACED | WCONTINUED);
    	    if (w == 0) {
    	        cout << "   " << pid << "   Running" << endl;
    	    }
    	    else if (WIFEXITED(status)) {
    	        cout << "   " << pid << "  Exited with status " << WEXITSTATUS(status) <<  endl;
    	    }
    	    else if (WIFSIGNALED(status)) {
    	        cout << "   " << pid << "  Killed by signal " <<  WTERMSIG(status) <<  endl;
	        }
	        else if (WIFSTOPPED(status)) {
	            cout << "   " << pid << "  Stopped by signal " << WSTOPSIG(status) <<  endl;
	        }
	        else if (WIFCONTINUED(status)) {
	            cout << "   " << pid << "  Continued" << endl;
	        }
		}
    }
}

void list_func_v2 (vector<int> pids) {
	 pid_t w, pid;
	 int status;
	 if (pids.empty()) {
	 	zmq::message_t reply(100);
	 	memcpy(reply.data(), "No child processes.\n", 100);
	 	socket.send(reply);
	 } else {
	 	zmq::message_t reply(100);
	 	memcpy(reply.data(), "   pid:     status: \n", 100);
	 	socket.send(reply);

		for (auto it = pids.begin(); it != pids.end(); it++){
			pid = *it;
			w = waitpid((pid_t)pid, &status, WNOHANG | WUNTRACED | WCONTINUED);
			if (w == 0) {
				string str = "   "+to_string(pid)+"   Running";
				const void *a = str.c_str();
				memcpy(reply.data(), a, 100);
				socket.send(reply);
			} else if (WIFEXITED(status)) {
				string str = "   "+to_string(pid)+"   Exited with status "+to_string(WEXITSTATUS(status));
				const void *a = str.c_str();
				memcpy(reply.data(), a, 100);
				socket.send(reply);
			} else if (WIFSIGNALED(status)) {
				string str = "   "+to_string(pid)+"  Killed by signal "+to_string(WTERMSIG(status));
				const void *a = str.c_str();
				memcpy(reply.data(), a, 100);
				socket.send(reply);
			} else if (WIFSTOPPED(status)) {
				string str = "   "+to_string(pid)+"  Stopped by signal "+to_string(WSTOPSIG(status));
				const void *a = str.c_str();
				memcpy(reply.data(), a, 100);
				socket.send(reply);
			} else if (WIFCONTINUED(status)) {
				string str = "   "+to_string(pid)+"  Continued";
				const void *a = str.c_str();
				memcpy(reply.data(), a, 100);
				socket.send(reply);
			}
		}
	}
}
