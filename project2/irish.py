#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3.6
# -*- coding: UTF-8 -*-

### LEVEL 2 VERSION CLIENT CODE

import sys  # for parsing cmd line args
import time	# for printing every 5 seconds
import zmq	# for client work


# Socket to talk to server
context = zmq.Context()
socket = context.socket(zmq.REQ)

#if len(sys.argv) > 1:	# if in level 2 version
machine = sys.argv[1]	# get cmd line arg for machine to connect to
port = 62600

print("Attempting to connect to %s on port 62600..." %machine)
socket.connect("tcp://%s:62600" %machine)
print("Connected.")
#socket.setsockopt_string(zmq.SUBSCRIBE, '')

while 1:
	command = input(">  ")
	socket.send_string(command)
	message = socket.recv_string()
	print(message)
	time.sleep(1)

#if __name__ == "__main__":
 #   print("Not implemented.")   

