// NETID(s): mzanardo, mryan29

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <string>
#include <iostream>
#include <vector>
using namespace std;

// Current issues: Feb12 6:00pm
// child_pids are never added to pid vector, list not working
// think it has something

// Globals
vector<int> pids;

int main(int argc, char *argv[]) {

    int counter = 0;
    int child_pid;

	cout << "Welcome to irish.\n";

	while(1) {
        cout << ">  ";	// simulate terminal prompt

        // initialize input array and read in from stdin
        char input[100];
        fgets(input, 50, stdin);

        // initialize args and use spaces/newline as delimiters
        char * args;

        args = strtok(input, "\n");
        args = strtok(input, " ");

        // if user inputs 'bg'
        if (strcmp(args , "bg") == 0) {
            char *myargs[3];
            args = strtok(NULL, " ");
            myargs[0] = args;			// myargs[0]: python call
            args = strtok(NULL, " ");
            myargs[1] = args;			// myargs[1]: name of python program to run
			args = strtok(NULL, " ");
			myargs[2] = args;			// myargs[2]: process piping frequency

            myargs[3] = NULL;			// marks end of array

            int rc = fork();

            if (rc < 0) {       // Fork failed
                cerr << "Fork failed" << endl;
                exit(EXIT_FAILURE);
            }

            else if (rc == 0) {     // Child process
                child_pid = getpid();
                pids.push_back(child_pid);
                cout << "Child process pid: " << child_pid << endl;
                counter++;
//                pids.push_back(child_pid);
                execvp(myargs[0], myargs);
//                cout << "Child PID: " << child_pid << endl;
  //              pids.push_back(child_pid);		// attempting to add pids to pids vector
            }

/*           else {      // parent process
                int wc = wait(NULL);
                cout << "Parent process pid: " << getpid() << endl;
            } */
//            pids.push_back(child_pid);

			// current issue: this and everything below  runs and prints before the above child proccess
			// conditional and before the child pid is added to the pids vector
			cout << "attempting to print out pids vector...\n";
			if (pids.empty()) { cout << "No pids in pid vector.\n"; }
			for (auto i = pids.begin(); i != pids.end(); ++i) {
			    std::cout << *i << ' ';
			}
            continue;
        }

        if (strcmp(args , "list") == 0) {       // Not working.
        	if (pids.empty()) {	cout << "No pids in pid vector.\n"; }
            for (auto it = pids.begin(); it != pids.end(); it++) {
                cout << "pid: " << *it << endl;
            }
        }

        if (strcmp(args , "fg") == 0) {
            int pid = atoi(strtok(NULL, " "));
            pid = (pid_t)pid;
            pids.push_back(pid);
            pid_t wc = waitpid(pid, NULL, 0);
            cout << "Return code: " << wc << endl;
            if (errno != 0) {
                cout << strerror(errno) << endl;
            }
        }

        if (strcmp(args , "signal") == 0) {
            int pid = atoi(strtok(NULL, " "));
            char * sig = strtok(NULL, " ");
            int s;
            if (strcmp(sig , "SIGHUP") == 0) {
                s = kill(pid, 1);
            }

            if (strcmp(sig , "SIGINT") == 0) {
                s = kill(pid, 2);
            }

            if (strcmp(sig , "SIGQUIT") == 0) {
                s = kill(pid, 3);
            }

            if (strcmp(sig , "SIGSTOP") == 0) {
                s = kill(pid, 23);
            }

            if (strcmp(sig , "SIGKILL") == 0) {
                s = kill(pid, 9);
            }

            if (strcmp(sig , "SIGALARM") == 0) {
                s = kill(pid, 14);
            }

            if (strcmp(sig , "SIGCONT") == 0) {
                s = kill(pid, 25);
            }

            if (s == 0) {
                cout << "Success." << endl;
            }
            else if (errno != 0 ) {
                cout << strerror(errno) << endl;
            }

            continue;
        
        }

        if (strcmp(args , "stop") == 0) {
            int pid = atoi(strtok(NULL, " "));
            int s = kill(pid, 23);
            
            if (s == 0) {
                cout << "Process " << pid << " paused." <<  endl;
            }
            else if (errno != 0 ) {
                cout << strerror(errno) << endl;
            }
        }

        if (strcmp(args , "continue") == 0) {
            int pid = atoi(strtok(NULL, " "));
            int s = kill(pid, 25);
            
            if (s == 0) {
                cout << "Process " << pid << " continued." << endl;
            }
            else if (errno != 0 ) {
                cout << strerror(errno) << endl;
            }
        
        }

        if (strcmp(args , "quit") == 0) {
            // terminate processes here
            exit(1);
        }


    }

    return 0;
}

