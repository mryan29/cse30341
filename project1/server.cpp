/* Meg Ryan (mryan29) and Beatriz Zanardo (mzanardo)
reference:
		http://zguide.zeromq.org/cpp:interrupt
		http://zguide.zeromq.org/cpp:hwserver
		http://zguide.zeromq.org/cpp:wuserver (in handout)
assigned port ranges:
	Meg			62600 - 62699
	Beatriz		63900 - 63999

*/

#include <iostream>
#include <csignal>
#include <zmq.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fstream>
#include <string>
#include <sys/stat.h>	// for checking file size
#include <cstring>		// for checking file size (comparing 120 with stat)
#include <ctype.h>		// for checking non-ascii chars
#include <unistd.h>		// for access()
using namespace std;

/* Function Definitions */
void mySignalHandler(int);
bool errorCheck(string);

/* Global Variables */
int interrupted = 0;
ifstream myfile;


void mySignalHandler (int sig) {
	cout << "Signal Received: " << sig << endl;

	// cleanup and close stuff here
    interrupted = 1;    // ZMQ handles calling the variables' destructors and cleaning things up
    cout << "Exiting..." << endl;

    // terminate program

    exit(sig);
}

bool errorCheck(string filename) {


	// check if file exists
	ifstream myfile(filename);
	if (!myfile.good()) {
        if (access(filename.c_str(), 0) == -1) {
            cout << "Error: File does not exist.\n";
            return false;
        }

        // check file permissions
        if (access(filename.c_str(), R_OK) != 0) {
            myfile.close();
            cout << "Error: Do not have permission to this file.\n";
            return false;
        }
    }

	// check if file opens
	myfile.open(filename);
	if (!myfile.is_open()) {
        myfile.close();
        cout << "Error: Could not open file.\n";
        return false;
    }

	// check file size
	struct stat st;
	stat(filename.c_str(), &st);
	if (st.st_size > 120) {
        myfile.close();
        cout << "File exceeds maximum size.\n";
        return false;
    }

    return true;

}

int main (int argc, char* argv[]) {

	/* Parse the input arguments and check for a port */
	string port;
    if (argc > 1 ) {
        port = argv[1];
    }
    else {
        port = "62600";
    }
    string url = "tcp://*:" + port;

	/* Catch Control-C via the signal handler */
	signal(SIGINT, mySignalHandler);

	/* Set up the ZMQ Socket as a Publisher on the right port */
	zmq::context_t context(1);
	zmq::socket_t publisher(context, ZMQ_PUB);
	publisher.bind(url);

    cout << "Welcome to the Multi-Process Message Broadcast System" << endl;
    cout << "Now publishing on port " << port << endl;

	while (!interrupted) {
		/* Wait for user input */
        string input;
        cin >> input;
//		getline(cin, input);		// attempting to handle multiple commands (i.e. help quit)
//		istringstream stream(input);


		/* Parse what user typed */
        if (input == "help") {
            cout << "The commands for MP-MBS are as follows:" << endl;
            cout << "   help          This command" << endl;
            cout << "   send XXX      Send the file XXX out to all subscribed clients" << endl;
            cout << "   quit          Exit this code" << endl;

        }
        else if (input == "quit") {
            cout << "Exiting..." << endl;
            interrupted = 1;
            break;
        }

		/* If send command, try to read file - check success/content */
        else if (input == "send") {
			string filename;
        	cin >> filename;		// get filename from user input
			if (errorCheck(filename) == true) {;	// check file for errors
				myfile.open(filename);
				string line;

        	    zmq::message_t message(120);

	            string message_str = "";
				bool nonAsciiFound = false;
				string confirm;
				cout << "\nThe following message will be sent:\n";

				/* Reading from file */
				while (getline(myfile, line)) { 	// parse file, construct a message string and print to user

					// Checks for non-ascii text, deletes it from output if found
					// reference: http://www.cplusplus.com/reference/string/string/erase/
					for (unsigned int i = 0; i < line.length(); i++) {
						if (!isascii(line[i])) { 
                            line.erase(i, 2); 
                            nonAsciiFound = true; 
                        }
					}

					cout << line << endl;
    	            message_str = message_str + line + '\n';
        	    }

				if (nonAsciiFound) { 
                    cout << "**Warning: Non-Ascii text found in file and removed from output.**\n"; 
                }
                cout << endl;

				// construct message to be sent
            	snprintf ((char *) message.data(), 120, "%s", message_str.c_str());

				cout << "Are you sure you want to send this message? Enter 'YES' to continue" << endl;
		    	cin >> confirm;
				if (confirm == "YES") {
					publisher.send(message);
					cout << "Message sent." << endl;
				}
				else {
					cout << "Message not sent." << endl;
				}

				myfile.close();
            }
        }

        /* Repeat */
        else {
            cout << "Unknown command. Please enter a new command." << endl;
        }
	}

	return 0;

}
