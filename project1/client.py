#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3.6
# -*- coding: UTF-8 -*-

# Meg Ryan (mryan29) and Beatriz Zanardo (mzanardo)

import sys	# for parsing cmd line args
import zmq	# for client work
import time # for sleep function and timestamp

# Socket to talk to server
context = zmq.Context()
socket = context.socket(zmq.SUB)

if len(sys.argv) > 1:
    port = sys.argv[1] 	# get command line arg for port number to connect to
else:
    port = 62600

# Port number error checking
if not (int(port) >= 62600) and (int(port) <= 62699):     # check first range or ports
    if not (int(port) >= 63900) and (int(port) <= 63999):     # check second range
        print("Invalid port number. Connecting to a valid port.")
        port = 62600

print("Listening to server...")
socket.connect("tcp://localhost:%s" %port)
print("Connected to port: %s" %port)
socket.setsockopt_string(zmq.SUBSCRIBE, '')

# for reference: 
# http://zguide.zeromq.org/py:hwserver
# https://gist.github.com/doctaphred/2a41d8f1eab8d5ee9b507f9496c7a9ac
while 1:
    message = socket.recv_string()
    print("Message received at %s:" %time.strftime("%m/%d/%Y, %H:%M:%S", time.localtime()))
    print(message)
    time.sleep(1)


if __name__ == "__main__":
	print("Not implemented.")	

