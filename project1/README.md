See https://nd-cse-30341-sp18.gitlab.io/Project-1/

To Do - as of Feb 1 12:00pm (according to rubric)
- [1 pt] Open file in different directory
- Error checking:
	- [4 pts] Startup
		- [1 pt] Bad port number?
		- [1 pt] Unspecified port number?
		- [2 pts] Are the error messages informative for each?
	- [1 pt] Graceful handling of multiple command (ex. help quit)
			
