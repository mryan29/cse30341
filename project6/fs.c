
#include "fs.h"
#include "disk.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#define FS_MAGIC           0xf0f03410
#define INODES_PER_BLOCK   128
#define POINTERS_PER_INODE 5
#define POINTERS_PER_BLOCK 1024

struct fs_superblock {
	int magic;
	int nblocks;
	int ninodeblocks;
	int ninodes;
};

struct fs_inode {
	int isvalid;
	int size;
	int direct[POINTERS_PER_INODE];
	int indirect;
};

union fs_block {
	struct fs_superblock super;
	struct fs_inode inode[INODES_PER_BLOCK];
	int pointers[POINTERS_PER_BLOCK];
	char data[DISK_BLOCK_SIZE];
};

char inode_bitmap[100000];
char data_bitmap[100000];

int isMounted = 0;

// Helper functions

void clear_inodes() {
	union fs_block block;
	disk_read(0,block.data);
	int inode_blocks = block.super.ninodeblocks;

	for (int i = 0; i < inode_blocks; i++) {		// initialize inode bitmap
		disk_read(i + 1, block.data);
		for (int j = 0; j < INODES_PER_BLOCK; j++) {
			int index = (i * 128) + j;
			if (block.inode[j].isvalid == 1) {
				inode_bitmap[index] = 0;
				block.inode[j].isvalid = 0;
				block.inode[j].size = 0;
				for (int k = 0; k < POINTERS_PER_INODE; k++) {
					block.inode[j].direct[k] = 0;
				}
				block.inode[j].indirect = 0;
			}
		}
		disk_write(i + 1, block.data);
	}
}

void clear_data() {
	union fs_block block;
	disk_read(0,block.data);
	int nblocks = block.super.nblocks;
	
	for (int i = 0; i < nblocks; i++) {
		if (data_bitmap[i] == 1) {
			data_bitmap[i] = 0;
			disk_read(i, block.data);
			memset(block.data, 0, sizeof(block.data));
			disk_write(i, block.data);
		}
	}
}

void set_superblock() {
	union fs_block block;
	disk_read(0,block.data);
	block.super.magic = FS_MAGIC;
	int nblocks = disk_size();
	block.super.nblocks = nblocks;
	int inode_blocks = (nblocks/10) + 1;;
	block.super.ninodeblocks =  inode_blocks;
	block.super.ninodes = inode_blocks * INODES_PER_BLOCK;
	disk_write(0, block.data);
}

int find_free_inode() {
	union fs_block block;
	disk_read(0,block.data);
	int num = block.super.ninodes;

	for (int i = 1; i < num; i++) {
		if (inode_bitmap[i] == 0) {
			return i;
		}
	}

	return -1;
}

int find_free_data() {
	union fs_block block;
	disk_read(0,block.data);
	int num = block.super.nblocks;

	for (int i = 0; i < num; i++) {
		if (data_bitmap[i] == 0) {
			data_bitmap[i] = 1;
			return i;
		}
	}

	return -1;
}
void clear_data_block (int block_num) {
	union fs_block block;
	data_bitmap[block_num] = 0;
	disk_read(block_num, block.data);
	memset(block.data, 0, sizeof(block.data));
	disk_write(block_num, block.data);
}

void clear_indirect_block (int block_num) {
	union fs_block block;
	data_bitmap[block_num] = 0;
	disk_read(block_num, block.data);
	for (int i = 0; i < POINTERS_PER_BLOCK; i++) {
		if (block.pointers[i] != 0) {
			clear_data_block(i);
		}
	}
	disk_write(block_num, block.data);
}

// fs functions

int fs_format()
{
	if (isMounted == 1) {
		printf("Cannot format an already mounted disk.\n");
		return 0;
	}
	else {
		clear_inodes();
		clear_data();
		set_superblock();
		return 1;
	}
}

void fs_debug()
{
	union fs_block block;
	disk_read(0,block.data);

	if (isMounted == 0) {
		printf("Need to mount filesystem before debugging.\n");
		if (block.super.magic == FS_MAGIC) {
			printf("    magic number is valid.\n");
		}
	}

	else {
		printf("superblock:\n");		// print superblock info
		if (block.super.magic == FS_MAGIC) {
			printf("    magic number is valid.\n");
		}
		else {
			printf("magic number is not valid.\n");
		}
		printf("    %d blocks\n",block.super.nblocks);
		printf("    %d inode blocks\n",block.super.ninodeblocks);
		printf("    %d inodes\n",block.super.ninodes);

		int totalInodes = block.super.ninodes;

		for (int i = 0; i < totalInodes; i++) {		// print inode info
			if (inode_bitmap[i] == 1) {

				int block_num = 1 + (i / 128);
				int inode_index = i % 128;

				disk_read(block_num, block.data);

				printf("inode %d:\n", i);
				printf("	size: %d bytes\n", block.inode[inode_index].size);
				printf("	direct blocks: ");

				for (int j = 0; j < POINTERS_PER_INODE; j++) {		//  direct pointers
					if (block.inode[inode_index].direct[j] != 0) {
						printf("%d ", block.inode[inode_index].direct[j]);
					}
				}
				printf("\n");

				int indirect_block = block.inode[inode_index].indirect;		// indirect pointers
				if (indirect_block != 0) {
					printf("	indirect block: %d \n", indirect_block);
					printf("	indirect data blocks: ");
					disk_read(indirect_block, block.data);
					for (int k = 0; k < POINTERS_PER_BLOCK; k++) {
						if (block.pointers[k] != 0) {
							printf("%d ", block.pointers[k]);
						}
					}
				printf("\n");

				}
			}
		}
	}
}

int fs_mount()
{
	union fs_block block;
	disk_read(0, block.data);

	int inode_blocks = block.super.ninodeblocks;
	//int n = block.super.nblocks;
	int totalInodes = block.super.ninodes;


	for (int i = 0; i < inode_blocks; i++) {		// initialize inode bitmap
		disk_read(i + 1, block.data);
		for (int j = 0; j < INODES_PER_BLOCK; j++) {
			int index = (i * 128) + j;
			if (block.inode[j].isvalid == 1) {
				inode_bitmap[index] = 1;
			}
			else {
				inode_bitmap[index] = 0;
			}
		}
	}

	for (int i = 0; i < totalInodes; i++) {		// initialize data bitmap
			if (inode_bitmap[i] == 1) {

				int block_num = 1 + (i / 128);
				int inode_index = i % 128;

				disk_read(block_num, block.data);

				for (int j = 0; j < POINTERS_PER_INODE; j++) {		//  direct pointers
					int direct_block = block.inode[inode_index].direct[j];
					if (direct_block != 0) {
						data_bitmap[direct_block] = 1;
					}
				}

				int indirect_block = block.inode[inode_index].indirect;		// indirect pointers
				if (indirect_block != 0) {
					data_bitmap[indirect_block] = 1;
					disk_read(indirect_block, block.data);
					for (int k = 0; k < POINTERS_PER_BLOCK; k++) {
						if (block.pointers[k] != 0) {
							data_bitmap[block.pointers[k]] = 1;
						}
					}
				}
			}
		}

		/*for (int f = 0; f < n; f++) {
			printf("bitmap index: %d value: %d\n", f, data_bitmap[f]);
		}*/

	isMounted = 1;
	return 1;
}

int fs_create()
{
	int inode_num = find_free_inode();

	if (inode_num == -1) {
		printf("Operation failed: No space to create new inode. \n");
		return 0;
	}

	else {
		inode_bitmap[inode_num] = 1;
		int block_num = (inode_num / INODES_PER_BLOCK) + 1;
		int index = inode_num % INODES_PER_BLOCK;
		union fs_block block;
		
		disk_read(block_num, block.data);
		block.inode[index].isvalid = 1;
		block.inode[index].size = 0;
		disk_write(block_num, block.data);

		printf("inode number %d create. \n", inode_num);
		return inode_num;
	}

	return 0;
}

int fs_delete( int inumber )
{
	union fs_block block;
	disk_read(0, block.data);

	if (block.super.ninodeblocks <= inumber) {
		printf("inode number out of range\n");
		return 0;
	}

	if (inode_bitmap[inumber] == 0) {
		printf("inode doesn't exist. \n");
		return 0;
	}

	inode_bitmap[inumber] = 0;

	int block_num = (inumber / INODES_PER_BLOCK) + 1;
	int index = inumber % INODES_PER_BLOCK;


	disk_read(block_num, block.data);

	block.inode[index].isvalid = 0;
	block.inode[index].size = 0;
	for (int i = 0; i < POINTERS_PER_INODE; i++) {
		if (block.inode[index].direct[i] != 0) {
			clear_data_block(i);
		}
	}

	if (block.inode[index].indirect != 0) {
		clear_indirect_block(block.inode[index].indirect);
	}

	disk_write(block_num, block.data);

	return 1;
}

int fs_getsize( int inumber )
{
	union fs_block block;
	disk_read(0, block.data);

	if (block.super.ninodeblocks <= inumber) {
		printf("inode number out of range\n");
		return -1;
	}

	int block_num = (inumber/INODES_PER_BLOCK) + 1;
	int index = inumber % INODES_PER_BLOCK;

	disk_read(block_num, block.data);
	if (block.inode[index].isvalid != 1) {
		printf("invalid inode. \n");
		return -1;
	}
	else {
		printf("inode %d size: %d", inumber, block.inode[index].size);
		return block.inode[index].size;
	}

	return -1;
}

int fs_read( int inumber, char *data, int length, int offset )
{
	union fs_block block;
	disk_read(0, block.data);

	if (block.super.ninodeblocks <= inumber) {
		printf("inode number out of range\n");
		return 0;
	}

	if (inode_bitmap[inumber] == 0) {
		printf("inode doesn't exist. \n");
		return 0;
	}

	union fs_block new_block;

	int block_num = (inumber/INODES_PER_BLOCK) + 1;
	int index = inumber % INODES_PER_BLOCK;
	int indirect = 0;
	int indirect_num;

	disk_read(block_num, block.data);

	int offset_block = offset / 4096;
	int data_read = length;
	int data_block;
	if (offset_block < 5) {
		data_block = block.inode[index].direct[offset_block];
	}
	else {
		indirect_num = block.inode[index].indirect;
		indirect = 1;
		offset_block = (offset - (5*4096)) / 4096;
		disk_read(indirect_num, block.data);
		data_block = block.pointers[offset_block];
	}

	while (data_read > 0) {
		if (data_block != 0) {
			disk_read(data_block, new_block.data);

			if (data_read > 4096) {
				memcpy(data, new_block.data, 4096);
				data_read = data_read - 4096;
			}

			else {
				memcpy(data, new_block.data, data_read);
				data_read = 0;
				break;
			}
		}
		else {
			break;
		}

		if (indirect == 0) {		// still reading from direct blocks
			offset_block++;
			if (offset_block > 4) {		// need to move to indirect block
				indirect = 1;
				offset_block = -1;
				indirect_num = block.inode[index].indirect;
			}
			else {
				data_block = block.inode[index].direct[offset_block];
			}
		}
		if (indirect == 1) {
			offset_block++;
			disk_read(indirect_num, block.data);
			data_block = block.pointers[offset_block];
		}
	}

	return (length - data_read);
}

int fs_write( int inumber, const char *data, int length, int offset )
{
	union fs_block block;
	disk_read(0, block.data);

	if (block.super.ninodeblocks <= inumber) {
		printf("inode number out of range\n");
		return 0;
	}

	union fs_block new_block;

	int block_num = (inumber/INODES_PER_BLOCK) + 1;
	int index = inumber % INODES_PER_BLOCK;
	int indirect = 0;
	int indirect_num;

	disk_read(block_num, block.data);

	int offset_block = offset / 4096;
	int data_write = length;
	int data_block;
	if (offset_block < 5) {
		data_block = block.inode[index].direct[offset_block];
		if (data_block == 0) {
			data_block = find_free_data();
			block.inode[index].direct[offset_block] = data_block;
			disk_write(block_num, block.data);
		}
	}
	else {
		indirect_num = block.inode[index].indirect;
		if (indirect_num == 0) {
			indirect_num = find_free_data();
			block.inode[index].indirect = indirect_num;
			disk_write(block_num, block.data);
		}
		indirect = 1;
		offset_block = (offset - (5*4096)) / 4096;
		disk_read(indirect_num, block.data);
		data_block = block.pointers[offset_block];
		if (data_block == 0) {
			data_block = find_free_data();
			block.pointers[offset_block] = data_block;
			disk_write(indirect_num, block.data);
		}
	}

	while (data_write > 0) {
		disk_read(data_block, new_block.data);
		if (data_block == 0) {
			data_block = find_free_data();
			if (indirect == 1) {
				block.pointers[offset_block] = data_block;
			}
			else {
				block.inode[index].direct[offset_block] = data_block;
			}
		}
		if (data_write > 4096) {
			memcpy(new_block.data, data, 4096);
			data_write = data_write - 4096;
			disk_write(data_block, new_block.data);
		}

		else {
			memcpy(new_block.data, data, data_write);
			data_write = 0;
			disk_write(data_block, new_block.data);
			break;
		}


		if (indirect == 0) {		// still writting to direct blocks
			offset_block++;
			if (offset_block > 4) {		// need to move to indirect block
				indirect = 1;
				offset_block = -1;
				indirect_num = block.inode[index].indirect;
			}
			else {
				data_block = block.inode[index].direct[offset_block];
			}
		}
		if (indirect == 1) {
			offset_block++;
			disk_read(indirect_num, block.data);
			data_block = block.pointers[offset_block];
		}
	}		

	return (length - data_write);
}
