
#include "bitmap.h"

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

int iteration_to_color( int i, int max );
int iterations_at_point( double x, double y, int max );
void compute_image( struct bitmap *bm, double xmin, double xmax, double ymin, double ymax, int max );

// ADDED FOR PART 3
typedef struct pArgs {
	struct bitmap *bm;
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	double start;
	double end;
	int max;
}pArgs;

// Create a bitmap of the appropriate size.
//struct bitmap *bm;

//void * thread_compute_image (struct pArgs *p_args, pthread_t p);
void * thread_compute_image (void * args);
void show_help()
{
	printf("Use: mandel [options]\n");
	printf("Where options are:\n");
	printf("-m <max>    The maximum number of iterations per point. (default=1000)\n");
	printf("-x <coord>  X coordinate of image center point. (default=0)\n");
	printf("-y <coord>  Y coordinate of image center point. (default=0)\n");
	printf("-s <scale>  Scale of the image in Mandlebrot coordinates. (default=4)\n");
	printf("-W <pixels> Width of the image in pixels. (default=500)\n");
	printf("-H <pixels> Height of the image in pixels. (default=500)\n");
	printf("-o <file>   Set output file. (default=mandel.bmp)\n");
	printf("-n <num>	Specify the number of threads (default=1)\n");	// ADDED
	printf("-h          Show this help text.\n");
	printf("\nSome examples are:\n");
	printf("mandel -x -0.5 -y -0.5 -s 0.2\n");
	printf("mandel -x -.38 -y -.665 -s .05 -m 100\n");
	printf("mandel -x 0.286932 -y 0.014287 -s .0005 -m 1000\n\n");
}

int main( int argc, char *argv[] )
{
	char c;

	// These are the default configuration values used
	// if no command line arguments are given.

	const char *outfile = "mandel.bmp";
	double xcenter = 0;
	double ycenter = 0;
	double scale = 4;
	int    image_width = 500;
	int    image_height = 500;
	int    max = 1000;
	int	nThreads = 1; // ADDED FOR PART 3

	// For each command line argument given,
	// override the appropriate configuration value.

	while((c = getopt(argc,argv,"x:y:s:W:H:m:o:n:h"))!=-1) {
		switch(c) {
			case 'x':
				xcenter = atof(optarg);
				break;
			case 'y':
				ycenter = atof(optarg);
				break;
			case 's':
				scale = atof(optarg);
				break;
			case 'W':
				image_width = atoi(optarg);
				break;
			case 'H':
				image_height = atoi(optarg);
				break;
			case 'm':
				max = atoi(optarg);
				break;
			case 'o':
				outfile = optarg;
				break;
			case 'n':		// ADDED FOR STEP THREE
				nThreads = atoi(optarg);
				break;
			case 'h':
				show_help();
				exit(1);
				break;
		}
	}

	// Display the configuration of the image.
	printf("mandel: x=%lf y=%lf scale=%lf max=%d outfile=%s\n",xcenter,ycenter,scale,max,outfile);

	// Create a bitmap of the appropriate size.
	//struct bitmap *bm = bitmap_create(image_width,image_height);
	struct bitmap *bm = bitmap_create(image_width,image_height);

	// Fill it with a dark blue, for debugging
	bitmap_reset(bm,MAKE_RGBA(0,0,255,0));


	// EDITED FOR STEP THREE
	if (nThreads != 1) {
	
		pArgs threadArgs[nThreads];	// create array of nThreads different structs
		pthread_t pts[nThreads];
		int j;
        for (j = 0; j < nThreads; j++) {
			threadArgs[j].bm	= bm;
			threadArgs[j].xmin 	= xcenter-scale;
			threadArgs[j].xmax	= xcenter+scale;
			threadArgs[j].ymin 	= ycenter-scale;
			threadArgs[j].ymax 	= ycenter+scale;
			threadArgs[j].start = (j*image_height)/nThreads;
			if (j == nThreads - 1) { threadArgs[j].end 	= image_height; }
			else { threadArgs[j].end 	= (j+1*(image_height/nThreads)); }
			//threadArgs[j].ymin 	= (ycenter-scale) + ((j*scale)/2.0);
			//threadArgs[j].ymax	= threadArgs[j].ymin + (scale/2.0);
			threadArgs[j].max	= max;
			
			pthread_create(&pts[j], NULL, thread_compute_image, (void *)&threadArgs[j]);
		}
		
		
		
		for (j = 0; j < nThreads; j++) {
			pthread_join(pts[j], NULL);
		}
		
		if(!bitmap_save(bm,outfile)) {
			fprintf(stderr,"mandel: couldn't write to %s: %s\n",outfile,strerror(errno));
			return 1;
		}
	
	} else {
			
		// Compute the Mandelbrot image
		compute_image(bm,xcenter-scale,xcenter+scale,ycenter-scale,ycenter+scale,max);

	
		// Save the image in the stated file.
		if(!bitmap_save(bm,outfile)) {
			fprintf(stderr,"mandel: couldn't write to %s: %s\n",outfile,strerror(errno));
			return 1;
		}
	}
	return 0;
}

/*
Compute an entire Mandelbrot image, writing each point to the given bitmap.
Scale the image to the range (xmin-xmax,ymin-ymax), limiting iterations to "max"
*/

void compute_image( struct bitmap *bm, double xmin, double xmax, double ymin, double ymax, int max )
{
	int i,j;

	int width = bitmap_width(bm);
	int height = bitmap_height(bm);
	
	// For every pixel in the image...

	for(j=0;j<height;j++) {

		for(i=0;i<width;i++) {

			// Determine the point in x,y space for that pixel.
			double x = xmin + i*(xmax-xmin)/width;
			double y = ymin + j*(ymax-ymin)/height;

			// Compute the iterations at that point.
			int iters = iterations_at_point(x,y,max);

			// Set the pixel in the bitmap.
			bitmap_set(bm,i,j,iters);
		}
	}
	
}

// ADDED FOR STEP THREE
void * thread_compute_image(void * args) {
	pArgs * p_args;
	p_args = (pArgs *) args;
	
	int i,j;
	int width 	= bitmap_width(p_args->bm);
	int height 	= bitmap_height(p_args->bm);
	int ymin	= p_args->ymin;
	int ymax	= p_args->ymax;
	int xmin	= p_args->xmin;
	int xmax	= p_args->xmax;
	int strt		= p_args->start;
	int end			= p_args->end;
	
	for(j=strt;j<end;j++) {

		for(i=0;i<width;i++) {
			// Determine the point in x,y space for that pixel.
			double x = xmin + i*(xmax-xmin)/width;
			double y = ymin + j*(ymax-ymin)/height;
			// Compute the iterations at that point.
			int iters = iterations_at_point(x,y,p_args->max);
			// Set the pixel in the bitmap.
			bitmap_set(p_args->bm,i,j,iters);
		}
	}
	return NULL;
}

/*
Return the number of iterations at point x, y
in the Mandelbrot space, up to a maximum of max.
*/

int iterations_at_point( double x, double y, int max )
{
	double x0 = x;
	double y0 = y;

	int iter = 0;

	while( (x*x + y*y <= 4) && iter < max ) {

		double xt = x*x - y*y + x0;
		double yt = 2*x*y + y0;

		x = xt;
		y = yt;

		iter++;
	}

	return iteration_to_color(iter,max);
}

/*
Convert a iteration number to an RGBA color.
Here, we just scale to gray with a maximum of imax.
Modify this function to make more interesting colors.
*/

int iteration_to_color( int i, int max )
{
	int gray = 255*i/max;
	return MAKE_RGBA(gray,gray,gray,0);
}




