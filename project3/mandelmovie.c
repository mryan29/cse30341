// NETID(s): mzanardo, mryan29

#include "bitmap.h"

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {

	char * N_str;

	if (argc > 1) {
		N_str = argv[1];
	}
	else {
		N_str = "2";
	}
	int N = atoi(N_str);
	char * myargs[8];
	double s = 2;
	
	myargs[0] = strdup("./mandel");
	

	char s_str[50];
	char file_num[50];
	
	sprintf(s_str, "%f", s);
	
	// Initialize command line options

	myargs[1] = strdup("-x  0.2869325 -n 4");
	myargs[2] = strdup("-y 0.0142905");
	myargs[3] = strdup("-W 1204");
	myargs[4] = strdup("-H 1024 -m 1000");
	char * scale = strdup("-s ");
    int i, j;

	for (i = 0; i < 50; i++) {
		for (j = 0; j < N; j++) {
			s = s - 0.0399998;		// decrement scale
			if (s < 0) {
				s = 0.000001;
			}
			memset(s_str,0,strlen(s_str));
			sprintf(s_str, "%lf", s);
			char * str = strdup(scale);
			myargs[5] = strcat(str, s_str);		// store current -s value
			sprintf(file_num, "-o mandel%d.bmp", i + j);		// store output file name
			myargs[6] = strdup(file_num);
			myargs[7] = NULL;
			printf("\n");

			int rc = fork();
			if (rc < 0) {
				fprintf(stderr, "fork failed\n");
				exit(1);
			}
			else if (rc == 0){
				execvp(myargs[0], myargs);
			}
		}
		int wc = wait(NULL);		// wait for a process to finish
		i = i + N - 1;
		N = 1;
	}
    return 0;
}
